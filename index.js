const http = require("http");
//
const port = 3000;

const server = http.createServer((req, res) => {
  // Accessing the 'greeting' route returns a message of 'Hello World'
  if (req.url == "/login") {
    res.writeHead(200, { "Content-Type": "html" });
    res.end("<h1>Welcome to the login page.</h1>");
  } else {
    res.writeHead(404, { "Content-Type": "html" });
    res.end("<p>I'm sorry the page you looking for cannot be found.</p>");
  }
});

server.listen(port);

console.log(`Listening to port: ${port}`);
