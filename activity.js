// 1. What directive is used by Node.js in loading the modules it needs?

// - It needs the "require" function to include external modules that exist in separate files

// 2. What Node.js module contains a method for server creation?

//  - The http module contains the method for server creation.

// 3. What is the method of the http object responsible for creating a server using Node.js?

// - Using the module's createServer() method, we can create an HTTP server that listens to the requests on a specified port and gives back to the client.

// 4. What method of the response object allows us to set status codes and content types?

// - the writeHead() method

// 5. Where will console.log() output its contents when run in Node.js?

// - it will run in the terminal

// 6. What property of the request object contains the address's endpoint?

// - The "url"
